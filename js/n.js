var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
var points = studentsAndPoints.filter(function (value, index) {
  return index % 2 != 0;
});
var students = studentsAndPoints.filter(function (value, index) {
  return index % 2 == 0;
});
//N-1 Разделение студентов на 2 разных массива.
console.log('Список студентов:');
students.forEach(function (value, index) {
  console.log('Студент ' + value + ' набрал ' + points[index] + ' баллов');
});
//Вывести список студентов без использования цикла 

var max, student;
points.forEach(function (value, index) {
  if (!max || value > max) {
    max = value;
    student = students[index];
  }
});
console.log('Студент набравший максимальный балл набрал - ' + student, max);

//Задание 4. Добавление 30 баллов студентам «Ирина Овчинникова» и «Александр Малов».
points = points.map(function(value, i){
    if(students[i]=='Ирина Овчинникова' || students[i]=='Александр Малов'){
        return value+30;
    }
    else return value;
});

console.log("\nДобавлены баллы. Список студентов:");
students.forEach(function(value,i){
    return console.log("Студент %s набрал %d баллов", value, points[i]);
});

// Дополнительное задание: реализовать функцию getTop, которое принимает на вход число и возвращает массив в формате studentsAndPoints..
var topMassive = getTop(3);
topMassive.forEach(function(elements,i,arr){
    if(i%2==0)
			console.log('Студент '+arr[i]+' - '+arr[i+1]+' баллов');
});

function getTop(i){
	console.log("Top %d:", i);
    var mass = points.slice();
    mass.sort(function(a,b){
	   return -(a-b);
    });
    mass = mass.slice(0,i);
	mass.forEach(function(item, i, arr){
       
        if(arr[i*2] == arr[i*2-1]){  // Сравниваем баллы текущего элемента mass и предыдущего 
            var studentsName = arr[i*2-2];
            var index = students.indexOf(studentsName);// Определяем индекс данного студента в массиве students по имени
            index = points.indexOf(arr[i*2], index+1);
            studentsName = students[index];
            arr.splice(i*2, 0, studentsName);
        }
        else{
            
            index = points.indexOf(arr[i*2]);
            studentsName = students[index];
            arr.splice(i*2, 0, studentsName);
        }
	});
	return mass;
}